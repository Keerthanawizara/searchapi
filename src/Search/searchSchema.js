const mongoose = require('mongoose');
//get the Schema class
const Schema = mongoose.Schema;
const searchSchema  = new Schema({
    propertyNumber: [{ type: Schema.Types.ObjectId, ref: 'assesseeList' }],
    name:[{ type: Schema.Types.ObjectId, ref: 'assesseeList' }],
    cellphone:[{ type: Schema.Types.ObjectId, ref: 'assesseeList' }],
    emailAddress:[{ type: Schema.Types.ObjectId, ref: 'assesseeList' }],
    address: String
});
 const search =  mongoose.model('searchList', searchSchema, 'propertyList');
 
 module.exports = search;