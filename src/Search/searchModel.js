const searchSchema = require('./searchSchema');
const mongoose = require('mongoose');


const SearchRecord = async (req) => {
  let pageNo = req.payload.pageNo || 0
  let pageSize = req.payload.pageSize || 10
  const requestKeys = Object.keys(req.payload.search)[0]
  const requestValues = Object.values(req.payload.search)[0]
   let docs = await searchSchema.find({ [requestKeys]: { $regex: requestValues, $options: 'i' } }.populate('authors'),{ skip: pageNo, limit: pageSize });
  if(docs){
      return docs
  }
}
module.exports=SearchRecord